'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PersonalData extends Model {

    static get table() {
        return 'personal_datas'
    }

    user () {
        return this.belongsTo('App/Models/User')
    }
}

module.exports = PersonalData
