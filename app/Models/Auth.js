'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Auth extends Model {
    user () {
        return this.belongsTo('App/Models/User')
    }

    role () {
        return this.belongsTo('App/Models/Role')
    }

    som () {
        return this.belongsTo('App/Models/Som')
    }
}

module.exports = Auth
