'use strict'

class LoginController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
  }
}

module.exports = LoginController
