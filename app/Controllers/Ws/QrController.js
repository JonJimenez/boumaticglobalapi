'use strict'

class QrController {
  constructor ({ socket, request }) {
    this.socket = socket
    this.request = request
  }

  onMessage (message) {
    this.socket.broadcastToAll('message', message)
  }

  onClose (message) {
    this.socket.close()
  }

  onError (message) {
    console.log(message)
  }
}

module.exports = QrController
