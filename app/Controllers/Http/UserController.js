'use strict'

const { create } = require("../../Models/User")

const User = use('App/Models/User')
const Mail = use('App/Mail')
const Auth = use('App/Models/Auth')
const Token = use('App/Models/Token')
const PersonalData = use('App/Models/PersonalData')
const Som = use('App/Models/Som')
const Encryption = use('Encryption')
const jwt = use('jsonwebtoken')
const Env = use('Env')
const View = use('View')
const moment = use('moment')
const Helpers = use('Helpers')

const ROLES = {
    '1': 'Guest',
    '2': 'Owner',
    '3': 'Dealer',
    '4': 'Veterinary',
    '5': 'Operator'
}

class UserController {

    async logout({request, response, auth}) {
        try {
            console.log(await auth.check())
        } catch (error) {
            return response.send('Already logged out')
        }

        return response.status(200).json({ status: 200, msg: true });
    }

    async login({request, response, auth}) {
        const {email, password} = request.all()

        const emUser = await User.query().where('email',email)
            .where('active', true)
            .first()

        if (!emUser) 
            return response.status(401).json({ msg: 'The user with this email doesn\'t exist.'})
        
        emUser.active = true;
        await emUser.save()
        const admin = await emUser.auths().where('role_id', 1).first()

        if (!admin) {
            return response.status(401).json({ msg: 'El usuario no cuenta con alguna credencial de admin.'})
        }
        
        const token = await auth
            .attempt(email, password)

        return response.status(200).json({ status: 200, token: token, user: emUser})
    }

    async getAdminData({response, auth}) {
        try {
            await auth.check()
        } catch (error) {
            return response.send('Missing or invalid token')
        }

        const user = await auth.getUser()

        if (!user)
            return response.status(401).json({ status: 401, msg: 'No existe un usuario loggeado.'})

        const soms = (await user.auths().where('role_id', 1).select('som').fetch()).toJSON()

        const somNames = soms.map(e => e.som)

        const roles = ROLES

        return response.status(200).json({status: 200, user: user, soms: somNames, roles: roles})
    }

    async clientLogin({request, response, auth}) {
        const {email, code} = request.all()

        const emUser = await User.query().where('email',email)
            .first()

        if (!emUser) 
            return response.status(500).json({ msg: 'The user with this email doesn\'t exist.'})
        
        const password = code;
        
        let token = ''
        try {
            token = await auth.attempt(email, password)
        } catch(error) {
            return response.status(401).json({ status: 401, msg: 'Wrong Credentials or incorrect password.'})
        }

        return response.status(200).json({ status: 200, token: token, user: emUser })
    }

    async getKeychain({response, auth}) {
        try {
            await auth.check()
        } catch (error) {
            return response.send('Missing or invalid token')
        }

        const user = await auth.getUser()

        if (!user)
            return response.status(500).json({ msg: 'The user doesn\'t exist anymore.'})

        const auths = (await user.auths()
                                .with('som').where('role_id', '!=', 1).fetch()).toJSON()

        let keychain = {};

        for (const som of auths) {

            const mobile_token = jwt.sign({ 
                email: user.email,
                som: som.som.serial,
                role_id: som.role_id
            }, Env.get('APP_KEY'), { expiresIn: 86400000 })
    
            const mobile_tokenEncrypted = Encryption.encrypt(mobile_token)    
            const keySOMRole = `${som.som.serial}|${som.role_id}`
            console.log(som.som)
            console.log(som.role_id)
            keychain[keySOMRole] = mobile_tokenEncrypted
        }

        if (auths.length == 0) {
            const mobile_token = jwt.sign({ 
                email: user.email,
                role_id: 1
            }, Env.get('APP_KEY'), { expiresIn: 86400000 })
    
            const mobile_tokenEncrypted = Encryption.encrypt(mobile_token)
            const keySOMRole = `Guest|1`
            keychain[keySOMRole] = mobile_tokenEncrypted
        }

        return response.status(200).json({ status: 200, data: keychain })
    }

    async checkLogin({ response, auth }) {
        try {
            await auth.check()
        } catch (error) {
            return response.send('Missing or invalid token')
        }

        const user = await auth.getUser()

        return response.status(200).json(user)
    }

    async access({request, response}) {
        const email = request.input('email')
        const mobile = request.input('mobile', false)
        const personal = request.input('personal', false)

        if (!email)
            return await response.status(500).json({msg: 'The email isn\'t valid.'})
        
        const code = this.createCode(5);
        let user = null;
        let createdUser = await User.query().where('email', '=', email).first()
        if (!createdUser && !personal) {
            
            user = {
                password: code,
                email: email
            }

            createdUser = await User.create(user)

            await createdUser.save()

            return response.status(200).json({ status: 200, email: email })
        } else if (createdUser && !personal) {
            const pData = await (createdUser.personalDatas().first())
            if (!pData)
                return response.status(200).json({ status: 200, email: email, msg: 'Guest already registered but missing personal info.' })
        }else if (createdUser && personal) {
            const pData = await (createdUser.personalDatas().first())
            if (!pData && personal) {
                let data = request.only(['name', 'occupation', 'phone'])

                data.user_id = createdUser.id

                const personal_data = await PersonalData.create(data)

                personal_data.save()
            }
        }

        
        createdUser.password = code
        await createdUser.save()
        
        const now = new moment();

        const expirationDateTS = now.add(10, 'minutes').format('x')

        const token = jwt.sign({ 
            email: email, 
            code: code, 
            expiration_date: expirationDateTS, 
            active: true 
        }, Env.get('APP_KEY'), { expiresIn: 86400000 })
        const tokenEncrypted = Encryption.encrypt(token)

        const host = Env.get('NODE_ENV') == 'production' 
        ?  `${Env.get('HOSTNAME_PROD')}/`
        : `http://${request.hostname()}:4200/`
        const url = mobile ? `` :`${host}confirm-code/${ tokenEncrypted.split('/').join('%2F') }`

        if (!user) {
            user = {
                email: createdUser.email,
                confirmation_code: code,
                mobile: mobile
            }
        }

        const view = await View.render('mails.code', {user, url})

        await Mail.sendMail(createdUser.email, 'Boumatic Auth Demo Access Code', view, false)

        return response.status(200).json({ status: 200, hasData: true, email: email })
    }

    async invite({request, response, auth}) {

        try {
            await auth.check()
        } catch(error) {
            return await response.status(401).json({msg: 'This session has already expired.'})
        }

        const email = request.input('email')
        const som = request.input('som')
        const role_id = request.input('role_id')

        if (!email)
            return response.status(500).json({msg: 'The email isn\'t valid.'})
        
        let user = {};
        let selected_som = null;
        let createdUser = await User.query().where('email', '=', email).first()
        if (!createdUser) {
            
            user = {
                password: '12345678',
                email: email
            }

            createdUser = await User.create(user)

            await createdUser.save()
        }
        const hasSomeAuth = await createdUser.auths().where('som_id',som).first()

        selected_som = (await Som.find(som))

        console.log(email)
        console.log(ROLES[role_id])
        console.log(selected_som.serial)

        if (!selected_som)
                return response.status(500).json({msg: 'The SOM you tried to invite for is not valid.'})

        if (hasSomeAuth) {
            if (hasSomeAuth.role_id != 1) {
                return response.status(500).json({msg: 'This user has already another role (not guest) in this SOM'})
            }
            hasSomeAuth.role_id = role_id
            await hasSomeAuth.save()
        } else {
            
            const auth_info = {
                user_id: createdUser.id,
                role_id: role_id,
                som_id: selected_som.id,
                token: '',
                active: true
            }

            const createdAuth = await Auth.create(auth_info)

            await createdAuth.save()
        }

        const now = new moment();

        const expirationDateTS = now.add(10, 'minutes').format('x')

        const token = jwt.sign({ 
            email: email,
            expiration_date: expirationDateTS, 
            active: true 
        }, Env.get('APP_KEY'), { expiresIn: 86400000 })
        const tokenEncrypted = Encryption.encrypt(token)

        console.log(Env.get('HOSTNAME_PROD'))
        const host = Env.get('NODE_ENV') == 'production' 
        ?  `${Env.get('HOSTNAME_PROD')}/`
        : `http://${request.hostname()}:4200/`
        const url = `${host}login/${ tokenEncrypted.split('/').join('%2F') }`

        user.som = selected_som.serial
        user.role = ROLES[role_id]
        user.email = email
        const view = await View.render('mails.invite', {user, url})

        await Mail.sendMail(createdUser.email, 'Boumatic Auth Demo Invitation', view, true)

        return response.status(200).json({ status: 200, email: email })
    }

  async confirmCode({params, response}) {
        let token = params.token.split('%2F').join('/')
        token = Encryption.decrypt(token)
        let msg
        
        jwt.verify(token, Env.get('APP_KEY'), (err, decoded) => {
            if (err) {
                msg = err
            } else {
                msg = decoded
            }
        })

        if (msg.hasOwnProperty('email') && msg.hasOwnProperty('expiration_date') && msg.hasOwnProperty('active')){
            const expiration_date = msg.expiration_date
            const today = new moment().format('x')

            if (today > expiration_date)
                return response.status(401).json({msg: 'This signed route has already expired.'})
            
            const user = await User.query().where('email', msg.email).first()
            if (!user)
                return response.status(401).json({msg: 'The user doesn\'t exist anymore.'})

            user.active = true
            await user.save()

            return response.status(200).json({ status: 200, data: {email: msg.email} })
        }
        
        if (msg.hasOwnProperty('email') && msg.hasOwnProperty('code') 
        && msg.hasOwnProperty('expiration_date') && msg.hasOwnProperty('active')) {

            const expiration_date = msg.expiration_date
            const today = new moment().format('x')

            if (today > expiration_date)
                return response.status(401).json({msg: 'This signed route has already expired.'})

            if (!msg.active) {
                const user = await User.query().where('email', msg.email).first()
                if (!user)
                    return response.status(401).json({msg: 'The user doesn\'t exist anymore.'})

                user.active = true
                await user.save()
            }

            return response.status(200).json({ status: 200, data: {email: msg.email, code: msg.code} })
        } else {
            return response.status(401).send('Unauthorized')
        }

    }

    async checkToken({ request, response }) {
        let token = request.input('token')
        token = Encryption.decrypt(token)
        let msg
        
        jwt.verify(token, Env.get('APP_KEY'), (err, decoded) => {
            if (err) {
                msg = err
            } else {
                msg = decoded
            }
        })
        
        if (msg.hasOwnProperty('email')) {
            return response.status(200).json({ status: 200, data: msg.email })
        } else {
            return response.status(401).send('No autorizado')
        }


    }

    async checkUser({response, auth, params}) {
        
        let token = params.token.split('%2F').join('/')
        token = Encryption.decrypt(token)
        let msg
        
        jwt.verify(token, Env.get('APP_KEY'), (err, decoded) => {
            if (err) {
                msg = err
            } else {
                msg = decoded
            }
        })

        if (!msg.hasOwnProperty('email')) {
            return response.status(401).json({ msg: 'Invalid Token.'})
        }
        const email = msg.email

        if (!email) 
            return response.status(401).json({ msg: 'Invalid email address.'})

        const emUser = await User.query().where('email',email)
            .where('active', true)
            .first()

        if (!emUser) 
            return response.status(500).json({ msg: 'The user with this email doesn\'t exist.'})

        const tokenJWT = await auth.generate(emUser, true)

        return response.status(200).json({ msg: 'Valid User', status: true, token: tokenJWT })
    }

    async syncSOM({request, response, auth}) {

        try {
            await auth.check()
        } catch(error) {
            return await response.status(401).json({msg: 'This session has already expired.'})
        }

        const { serial, farm, country_id, timezone_id, address, initializer } = request.all()

        const somData = {
            serial: serial,
            farm: farm,
            country_id: country_id,
            timezone_id: timezone_id,
            address: address,
            initializer: initializer
        }

        const som = await Som.query().where('serial', serial).first()

        if (som)
            return response.status(401).json({ msg: 'SOM already initialized, contact your Boumatic retailer'})

        const newSOM = await Som.create(somData)

        await newSOM.save()

        return response.status(200).json({ msg: 'SOM created successfully', status: true, som_id: newSOM.id })
    }

    /*async getInviteToken({ request, params, response }) {
        const email = params.email;
        const som = await Som.find(1)

        let user = await User.query().where('email', email).first()

        if (!user) {
            const newUser = await User.create({
                email: email,
                active: true,
                password: '12345'
            })

            await newUser.save()

            user = newUser
        }

        const hasAuth = await Auth.query().where('user_id',user.id).first()

        if (!hasAuth) {
            const leAuth = await Auth.create({
                user_id: user.id,
                som_id: 1,
                role_id: 2,
                active: true,
                token: ''
            })

            await leAuth.save()
        }

        const now = new moment();

        const expirationDateTS = now.add(10, 'minutes').format('x')

        const token = jwt.sign({ 
            email: email,
            expiration_date: expirationDateTS, 
            active: true 
        }, Env.get('APP_KEY'), { expiresIn: 86400000 })
        const tokenEncrypted = Encryption.encrypt(token)

        const host = Env.get('NODE_ENV') == 'production' 
        ?  `${Env.get('HOSTNAME_PROD')}/`
        : `http://${request.hostname()}:4200/`
        const url = `${host}login/${ tokenEncrypted.split('/').join('%2F') }`

        user.som = 'bma0001'
        user.role = ROLES[2]
        const view = await View.render('mails.invite', {user, url})

        await Mail.sendMail(email, 'Boumatic Auth Demo Invitation', view, true)

        return response.status(200).json({ status: 200, email: email, token: tokenEncrypted })
    }*/

    async inviteOpen({params, request, response}) {

        const email = params.email
        let email_type = params.type

        const accepted_types = ['invitation', 'code']

        if (!email)
            return response.status(500).json({msg: 'The email isn\'t valid.'})

        if (!email_type)
            return response.status(500).json({msg: 'The email type isn\'t valid.'})

        email_type = email_type.toLowerCase()
        if (!accepted_types.includes(email_type))
            return response.status(500).json({msg: 'The email type isn\'t valid, try with \'invitation\' or \'code\'.'})
        
        let user = {};

        const now = new moment();

        const expirationDateTS = now.add(10, 'minutes').format('x')

        const token = jwt.sign({ 
            email: email,
            expiration_date: expirationDateTS, 
            active: true 
        }, Env.get('APP_KEY'), { expiresIn: 86400000 })
        const tokenEncrypted = Encryption.encrypt(token)

        const host = Env.get('NODE_ENV') == 'production' 
        ?  `${Env.get('HOSTNAME_PROD')}/`
        : `http://${request.hostname()}:4200/`
        const url = `${host}login/${ tokenEncrypted.split('/').join('%2F') }`

        const code = this.createCode(5);

        user.som = 'bma0001'
        user.role = ROLES[1]
        user.email = email
        user.confirmation_code = code
        user.mobile = false

        let view = null;

        if (email_type == accepted_types[0])
            view = await View.render('mails.invite', {user, url})
        else
            view = await View.render('mails.code', {user, url})

        await Mail.sendMail(email, 
            (email_type == accepted_types[0] ? 'Boumatic Auth Demo Invitation' : 'Boumatic Auth Demo Access Code'), 
            view, 
            email_type == accepted_types[0])

        return response.status(200).json({ status: 200, msg: 'Correo enviado con éxito' })
    }

    createCode(length) {
        var result           = '';
        var characters       = '0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }

}

module.exports = UserController
