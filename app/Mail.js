'use strict'

const Env = use('Env')
const nodemailer = use('nodemailer')
const { google } = use('googleapis')
const OAuth2 = google.auth.OAuth2
const Helpers = use('Helpers')
const fs = use('fs')

function getAccessToken () {
  const oauth2Client = new OAuth2(
    Env.get('GOOGLE_CLIENT'),
    Env.get('GOOGLE_SECRET'),
    Env.get('GOOGLE_REDIRECT')
  )

  oauth2Client.setCredentials({
    refresh_token: Env.get('GOOGLE_REFRESH_TOKEN')
  })

  return new Promise((resolve, reject) => {
    oauth2Client.getAccessToken()
      .then(resolve)
      .catch(reject)
  })
}

async function sendMail (to, subject, view, withplaystore) {
  const smtpTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      type: 'oauth2',
      user: 'soporte@solidusystems.com',
      clientId: Env.get('GOOGLE_CLIENT'),
      clientSecret: Env.get('GOOGLE_SECRET'),
      refreshToken: Env.get('GOOGLE_REFRESH_TOKEN'),
      accessToken: await getAccessToken()
    }
  })
  
  const logo = fs.readFileSync(Helpers.appRoot(`resources/assets/images/boumatic_logo.jpg`), 'base64')
  const playstore = fs.readFileSync(Helpers.appRoot(`resources/assets/images/playstore.png`), 'base64')
  
  const mailOptions = {
    from: '"Auth Boumatic" <admin@boumatic.com>',
    to,
    subject,
    generateTextFromHTML: true,
    html: view,
    attachments: withplaystore ? [{
      filename: 'logo.jpg',
      path: 'data:image/jpg;base64,' + logo,
      cid: 'logo'
    },
    {
      filename: 'playstore.png',
      path: 'data:image/png;base64,' + playstore,
      cid: 'playstore'
    }] 
    : [{
      filename: 'logo.jpg',
      path: 'data:image/jpg;base64,' + logo,
      cid: 'logo'
    }]
  }

  smtpTransport.sendMail(mailOptions)
}

module.exports.getAccessToken = getAccessToken
module.exports.sendMail = sendMail
