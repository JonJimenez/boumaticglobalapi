'use strict'

const Role = use('App/Models/Role')

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class RoleSeeder {
  async run () {
    let role = await Role.create({
      name: 'Guest'
    })

    await role.save()

    role = await Role.create({
      name: 'Owner'
    })

    await role.save()

    role = await Role.create({
      name: 'Dealer'
    })

    await role.save()

    role = await Role.create({
      name: 'Veterinary'
    })

    await role.save()

    role = await Role.create({
      name: 'Operator'
    })

    await role.save()
    
  }
}

module.exports = RoleSeeder
