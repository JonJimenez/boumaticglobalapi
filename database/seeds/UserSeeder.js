'use strict'
const User = use('App/Models/User')
const Auth = use('App/Models/Auth')

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class UserSeeder {
  async run () {
    /*const user = await User.create({
      username: 'i.salazar@gmail.com',
      password: '12345678',
      email: 'i.salazar@gmail.com',
      active: true
    })

    await user.save()

    const userAuth = await Auth.create({
      user_id: 1,
      token: '',
      som: 'bmadev5',
      role_id: 1,
      active: true
    })

    const userAuth2 = await Auth.create({
      user_id: 1,
      token: '',
      som: 'bma0002',
      role_id: 1,
      active: true
    })

    const userAuth3 = await Auth.create({
      user_id: 1,
      token: '',
      som: 'bma0003',
      role_id: 1,
      active: true
    })

    await user.auths().save(userAuth)*/
  }
}

module.exports = UserSeeder
