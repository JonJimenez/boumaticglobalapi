'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PersonalDataSchema extends Schema {
  up () {
    this.create('personal_datas', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('name', 100).notNullable()
      table.string('occupation', 100).notNullable()
      table.string('phone', 20).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.table('personal_datas', (table) => {
      table.dropForeign('user_id')
    })
    this.drop('personal_datas')
  }
}

module.exports = PersonalDataSchema
