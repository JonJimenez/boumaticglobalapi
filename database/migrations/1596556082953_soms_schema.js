'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SomsSchema extends Schema {
  up () {
    this.create('soms', (table) => {
      table.increments()
      table.string('serial', 20).notNullable().unique()
      table.string('farm', 50).notNullable()
      table.string('country_id', 100).notNullable()
      table.string('timezone_id', 50).notNullable()
      table.text('address').notNullable()
      table.string('initializer', 50).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('soms')
  }
}

module.exports = SomsSchema
