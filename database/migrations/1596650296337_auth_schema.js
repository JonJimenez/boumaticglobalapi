'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AuthSchema extends Schema {
  up () {
    this.create('auths', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('role_id').unsigned().references('id').inTable('roles')
      table.integer('som_id', 7).unsigned().references('id').inTable('soms')
      table.text('token', 'longtext').notNullable()
      table.boolean('active').notNullable().defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.table('auths', (table) => {
      table.dropForeign('user_id')
      table.dropForeign('role_id')
      table.dropForeign('som_id')
    })

    this.drop('auths')
  }
}

module.exports = AuthSchema
