'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/access', 'UserController.access')
Route.get('/confirmUser/:token', 'UserController.confirmCode')
//Route.post('/completeUser', 'UserController.changePassword')
Route.post('/getAdminData', 'UserController.getAdminData')
Route.post('/getKeychain', 'UserController.getKeychain')

Route.post('/logout', 'UserController.logout')

Route.post('/check/login', 'UserController.checkLogin')
//Route.post('/user-info', 'UserController.userInfo')

Route.post('/login', 'UserController.login')
Route.post('/client-login', 'UserController.clientLogin')

//Route.get('auth/:token', 'UserController.getToken')
Route.post('/check/', 'UserController.checkToken')


// V2.0 SERVICES:

Route.get('/checkUser/:token', 'UserController.checkUser')
Route.post('/registerSOM', 'UserController.syncSOM')
Route.post('/sendInvitation', 'UserController.invite')

Route.get('/invite/:email/:type', 'UserController.inviteOpen')

Route.any('*', ({ view }) => view.render('index'))